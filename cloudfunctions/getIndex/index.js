// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const result = await db.collection("index").get()
  if (result.errMsg == "collection.get:ok" && result.data.length > 0) {
    return {
      code: 0,
      data: result.data[0].img
    }
  } else {
    return {
      code: -1,
    }
  }
}