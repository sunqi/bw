// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()
exports.main = async (event, context) => {
  var mchId=event.mchId
  var outTradeNo=event.outTradeNo
  var nonceStr=event.nonceStr
  var score=event.score
  const wxContext = cloud.getWXContext()
  const userResult = await db.collection("user").where({
    openId: wxContext.OPENID
  }).get()
  if (userResult.errMsg == "collection.get:ok" && userResult.data.length > 0) {
     await db.collection("user").doc(userResult.data[0]._id).update({
      data:{
        score:(userResult.data[0].score+score)
      }
    })
  }
  const res = await cloud.cloudPay.queryOrder({
    "sub_mch_id" : mchId,
    "out_trade_no" : outTradeNo,
    "nonce_str":nonceStr
  })
  return res
}