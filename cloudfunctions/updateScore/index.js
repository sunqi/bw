// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const userResult = await db.collection("user").where({
    openId: wxContext.OPENID
  }).get()
  if (userResult.errMsg == "collection.get:ok" && userResult.data.length > 0) {
     var result=await db.collection("user").doc(userResult.data[0]._id).update({
      data:{
        score:(userResult.data[0].score-20)
      }
    })
    if(result.errMsg=="document.update:ok"){
      return {
        code:0
      }
    }else{
      return {
        code:-1
      }
    }
  }else{
    return {
      code:-1
    }
  }
  
}