var http = require('./lib/http.js')
var utils = require("./lib/utils.js")
App({
  onLaunch: function () {
    
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env: 'my-env-id',
        traceUser: true,
      })
    }
  },
  request: {
    reqLoading: http.reqLoading,
    req: http.req,
    reqCloudLoading: http.reqCloudLoading,
    reqCloud: http.reqCloud
  },
  globalData: {
    userInfo: "",
    systemInfo: "",
    isUpdateScore:false,
    accessToken:''
  },
 
  // 登录先检查是否授权，没有授权打开权限设置，然后登录
  login: function (cb) {
    var that = this
    console.log("login")
    wx.getSetting({
      success: (res) => {
        console.log("login" + JSON.stringify(res))
        if (JSON.stringify(res.authSetting) != "{}") {
          if (res.authSetting['scope.userInfo'] == false) {
            wx.showModal({
              title: '授权提醒',
              content: '未授权登录将不能使用此小程序相关功能，请授权',
              success: function (res) {
                wx.openSetting({
                  success: (res) => {
                    if (res.authSetting['scope.userInfo']) {
                      that.ttLogin(cb)
                    } else {
                      that.failShowToast()
                    }
                  },
                  fail: (res) => {
                    console.log("userInfo", userInfo)
                    that.failShowToast()
                  }
                })
              },
              fail: (res) => {
                that.failShowToast()
              }
            })
          } else {
            that.ttLogin(cb)
          }
        } else {
          console.log("res.authSetting=" + JSON.stringify(res.authSetting))
          that.ttLogin(cb)
        }
      },
      fail: (res) => {
        that.failShowToast()
      }
    })
  },
  failShowToast: function () {
    this.showToastMsg('获取授权失败')
  },
  showToastMsg: function (titleName) {
    wx.showToast({
      title: titleName,
      icon: "none"
    });
  },
  ttLogin: function (cb) {
    var that = this
    console.log("login is start");
    wx.login({
      success(loginRes) {
        console.log("login is " + JSON.stringify(loginRes));
        if (loginRes.errMsg=='login:ok') {
          console.log("getUserInfo is start");
          wx.getUserInfo({
            success(res) {
              console.log("rawData===" + JSON.stringify(res));
              that.request.reqCloudLoading("login", {
                nickName: res.userInfo.nickName,
                avatarUrl:res.userInfo.avatarUrl,
                gender:res.userInfo.gender
              }, function (resCloud) {
                console.log("userInfo===" + JSON.stringify(resCloud));
                if (resCloud) {
                  that.SetUserInfoMsg(resCloud.userInfo);
                  cb(resCloud.userInfo)
                } else {
                  that.showToastMsg("登录失败")
                }
              })
            },
            fail(res) {
              console.log("userInfo===fail");
            }
          });
        } else {
          that.showToastMsg("微信没有登录，请登录")
        }
      },
      fail(res) {
        console.log(`login调用失败`);
      }
    });
  },
  GetToken: function () {
    return wx.getStorageSync(utils.constants.TOKEN)
  },
  GetUserInfoMsg: function () {
    if (this.globalData.userInfo) {
      return this.globalData.userInfo
    }
    var userInfo = wx.getStorageSync(utils.constants.USERINFO)
    this.globalData.userInfo = userInfo
    return userInfo
  },
  SetUserInfoMsg: function (userInfo) {
    this.globalData.userInfo = userInfo;
    wx.setStorageSync(utils.constants.USERINFO, userInfo)
  },
  UpdateUserScore: function (score) {
    var userInfo = this.GetUserInfoMsg()
    userInfo.score = userInfo.score+score
    wx.setStorageSync(utils.constants.USERINFO, userInfo)
  },
  UpdateUserDecreaseScore: function (score) {
    var userInfo = this.GetUserInfoMsg()
    userInfo.score = userInfo.score-score
    wx.setStorageSync(utils.constants.USERINFO, userInfo)
  },
  IsLogin: function () {
    var token = this.GetUserInfoMsg()
    if (token) {
      return true
    } else {
      return false
    }
  }
})
