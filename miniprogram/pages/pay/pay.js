var app = getApp()
Page({


  data: {
    goldenNum: 0,
    curIndex:0,
    curItem:'',
    items: [{
      sale: "最超值8折",
      golden: "1280金币",
      giveGolden: "(加赠300)",
      desc: '可修复79次，每次需1.62元',
      money: "128.00元",
      moneyNum:128,
      score:1580,
      originalMoney: "原价:158.00元",
      selected: true,
    },
    {
      sale: "专业修复",
      golden: "5180金币",
      giveGolden: "(加赠1500)",
      desc: '可修复334次，每次需1.55元',
      money: "518.00元",
      moneyNum:518,
      score:6680,
      originalMoney: "原价:668.00元",
      selected: false
    }, {
      sale: "新手尝鲜",
      golden: "300金币",
      giveGolden: "",
      desc: '可修复15次，每次需2.00元',
      money: "30.00元",
      moneyNum: 30,
      score:300,
      originalMoney: "",
      selected: false
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.curItem=this.data.items[0]
    this.setData({
      goldenNum:app.GetUserInfoMsg().score
    })
  },


  onShow: function () {

  },
  bindPay:function(){
    var that=this
    // this.data.curItem.moneyNum*100
    var totalFee=1
    var score=this.data.curItem.score
      app.request.reqCloudLoading("unifiedOrder",{
        name: this.data.curItem.sale,
        totalFee:totalFee,
        nickName:app.GetUserInfoMsg().nickName
      },function(res){
        console.log("====",JSON.stringify(res))
        wx.requestPayment({
          nonceStr: res.result.nonceStr,
          package: res.result.payment.package,
          signType:'MD5',
          paySign: res.result.payment.paySign,
          timeStamp: res.result.payment.timeStamp,
          success:payRes=>{
            app.showToastMsg("支付成功")
            app.request.reqCloud("queryOrder",{
              mchId: res.result.subMchId,
              nonceStr:res.result.nonceStr,
              outTradeNo:res.outTradeNo,
              score:score
            },function(res){
              console.log("====",JSON.stringify(res))
             app.UpdateUserScore(score)
             app.globalData.isUpdateScore=true
             that.setData({
              goldenNum:app.GetUserInfoMsg().score
             })
            })
          },fail:res=>{
            console.error("fail==",JSON.stringify(res))
            app.showToastMsg("支付失败，请联系客服")
          }
        })
      })
  },
  bindItemClick: function (e) {
    console.log(JSON.stringify(e))
    var index = e.currentTarget.dataset.index
    if(index==this.data.curIndex){
        return
    }
    var item=this.data.items[index]
    this.data.curIndex=index
    this.data.curItem.selected=false
    item.selected=true
    this.data.curItem=item
    this.setData({
      items:this.data.items
    })
  }
})