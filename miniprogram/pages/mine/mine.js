var app = getApp()
Page({
  data: {
    isLogin: false,
    appType: 0,
    userInfo: "",
  },
  onLoad: function (options) {
    var userInfo = app.GetUserInfoMsg()
    console.log(userInfo)
    if (userInfo != null && userInfo != undefined && userInfo != "") {
      this.setData({
        userInfo: userInfo,
        isLogin: true
      })
    }
  },
  onManager: function () {
    console.log("tt.env.VERSION=:", tt.env.VERSION)
    if (tt.env.VERSION == "development") {
      tt.clearStorageSync()
    }
  },
  onShow: function () {
    if (app.globalData.isUpdateScore) {
      var userInfo = app.GetUserInfoMsg()
      if (userInfo != null && userInfo != undefined && userInfo != "") {
        this.setData({
          userInfo: userInfo,
          isLogin: true
        })
      }
      app.globalData.isUpdateScore = false
    }
  },
  bindGetUserInfo: function (e) {
    var that = this
    app.login(function (res) {
      console.log("login res: ", JSON.stringify(res))
      that.setData({
        isLogin: true,
        userInfo: res
      })
    })
  },
  clearData: function () {
    tt.clearStorageSync()
    app.showToastMsg("清理数据成功")
  },
  onShareAppMessage: function () {
    return {
      title: '一键老照片变清晰，黑白变彩色',
      path: '/pages/index/index'
    }
  },
  bindOrder: function () {
    if (app.IsLogin()) {
      tt.navigateTo({
        url: '/pages/order/order' // 指定页面的url
      });
    } else {
      tt.navigateTo({
        url: '/pages/mine/login/login' // 指定页面的url
      });
    }
  }
})