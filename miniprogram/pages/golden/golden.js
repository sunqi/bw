var app = getApp()
Page({

  data: {
    originalImage: '../../images/1.jpg',
    isBlackFirst: true,
    isInit: true,
    isHave: false,
    toPic: '',
    need: 20,
    totoal: 0,
    height: '',
    width: '',
    mode: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var path = options.path
    var mode = options.mode
    var screenHeight = wx.getSystemInfoSync().screenHeight
    var screenWidth = wx.getSystemInfoSync().screenWidth
    console.log("====", JSON.stringify(screenHeight))
    this.setData({
      originalImage: path,
      mode: mode,
      height: screenHeight - 100,
      width: screenWidth - 20,
      totoal: app.GetUserInfoMsg().score
    })
  },
  bindUpload: function () {
    var url = "https://aip.baidubce.com/rest/2.0/image-process/v1/colourize?access_token="
    if (this.data.mode == 1) {
      url = "https://aip.baidubce.com/rest/2.0/image-process/v1/image_definition_enhance?access_token="
    }
    var that = this
    var token = wx.getStorageSync('token')
    if (!token) {
      app.showToastMsg("分析失败....")
      return
    }
    wx.getFileSystemManager().readFile({
      filePath: this.data.originalImage,
      encoding: "base64",
      success: fileRes => {
        wx.showLoading({
          title: '加载中...',
        })
        app.request.req(url + token, {
          "image": fileRes.data,
        }, function (res) {
          var code = res.data.err_code
          if (!code) {
            if (that.data.mode == 1) {
              app.request.reqCloud("updateScore", {
                socre: 20
              }, function (scoreRes) {
                wx.hideLoading()
                app.UpdateUserDecreaseScore(20)
                app.globalData.isUpdateScore = true
                if (scoreRes.code == 0) {
                  wx.setStorageSync('originalImage', encodeURIComponent(fileRes.data))
                  wx.setStorageSync('image', encodeURIComponent(res.data.image))
                  wx.navigateTo({
                    url: '../result/result?mode=' + that.data.mode + '&path=' + that.data.originalImage,
                  })
                } else {
                  wx.hideLoading()
                  app.showToastMsg("请求失败")
                }
              })
            } else {
              wx.setStorageSync('originalImage', encodeURIComponent(fileRes.data))
              wx.setStorageSync('image', encodeURIComponent(res.data.image))
              wx.navigateTo({
                url: '../result/result?mode=' + that.data.mode + '&path=' + that.data.originalImage,
              })
            }

          } else {
            wx.hideLoading()
            app.showToastMsg("请求失败")
          }
        }, "POST")
      },
      fail: res => {
        console.log("readfile fail==", JSON.stringify(res))
        wx.hideLoading()
      }
    })

  },

  onShareAppMessage: function () {

  }
})