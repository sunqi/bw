const app = getApp()
Page({
  data: {
  
  },

  onLoad: function() {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

    this.accessTokenFunc()
  },
  bindClickImg:function(){
    if(app.IsLogin()){
      wx.navigateTo({
        url: '/pages/pay/pay',
      })
    }else{
      app.showToastMsg("请登录...")
      wx.switchTab({
        url: '/pages/mine/mine',
      })
    }
  },
  accessTokenFunc: function () {
    wx.cloud.callFunction({
      name: 'getBaiduToken',
      success: res => {
        console.log("==baiduAccessToken==" + JSON.stringify(res.result))
        app.globalData.accessToken= res.result.token
        wx.setStorageSync('token', res.result.token)
        this.setData({
          img:res.result.resultIndex.result.data
        })
      },
      fail: err => {
        wx.clearStorageSync("access_token")
        wx.showToast({
          icon: 'none',
          title: '调用失败,请重新尝试',
        })
        console.error('云函数baiduAccessToken调用失败：', err)
      }
    })
  },
   onShareAppMessage:function(){
    return {
      title: '一键老照片变清晰，黑白变彩色',
      path: '/pages/index/index'
    }
  },
})
