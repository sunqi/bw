// miniprogram/pages/img/img.js
Page({

  
  data: {
    img:''
  },

  onLoad: function (options) {
    var base64Image = decodeURIComponent(wx.getStorageSync('image'))
    var imgData = base64Image.replace(/[\r\n]/g, '')
    this.setData({
      img:"data:image/png;base64,"+imgData
    })
  },

 
})