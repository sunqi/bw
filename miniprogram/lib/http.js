
function reqCloud(url, data, cb, isLoading) {
  if (isLoading) {
    wx.showLoading({
      title: '加载中...',
    })
  }
  wx.cloud.callFunction({
    name:url,
    data:data,
    success: res => {
      if (isLoading) {
        wx.hideLoading()
      }
      if (res.errMsg == "cloud.callFunction:ok") {
        return cb(res.result)
      } else {
        return cb({
          code:-1,
          msg:res.errMsg
        })
      }
    },
    fail: err => {
      if (isLoading) {
        wx.hideLoading()
      }
      return cb({
        code: -1,
        msg: err
      })
    },
  })
}


function req(url, data, cb, method,isLoading) {
  if (isLoading) {
    wx.showLoading({
      title: '加载中...',
    })
  }
  for (var Key in data) {
    if (data[Key] === null) {
      delete data[Key];
    }
  }
  wx.request({
    url: url,
    data: data,
    method: method || 'GET',
    header: {
      "content-type": "application/x-www-form-urlencoded",
    },
    complete:function(res){
        wx.hideLoading()
    },
    success: function (res) {
      return typeof cb == "function" && cb(res)
    },
    fail: function () {
      return typeof cb == "function" && cb(false)
    }
  })
}

function reqLoading(url, data, cb, method) {
  req(url, data, cb,method, true);
}
function reqCloudLoading(url, data, cb, method){
  reqCloud(url, data, cb, true);
}
module.exports = {
  req: req,
  reqLoading: reqLoading,
  reqCloud: reqCloud,
  reqCloudLoading: reqCloudLoading
}