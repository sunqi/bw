
function fenToYuan(fen) {
  return parseInt(fen / 100) + "." + parseInt(fen % 100 / 10) + parseInt(fen % 100 % 10)
}
const data = [
  "北京", "上海", "广州", "深圳", "南京", "常州", "无锡", "苏州", "武汉", "合肥", "西安", "重庆", "厦门", "石家庄", "哈尔滨", "海口", "沈阳", "大连", "成都", "青岛", "天津", "长沙", "杭州"
];
const nickNames = [
  "半夜树洞", "不回头的诗河", "你有鸭脖子吗", "生而为人，", "寇寇", "June🍎", 
  "Frank0202", "aligato_", "天上白玉京_123", "予囚", "别慌小场面", "瑞本尊", 
  "Husel", "橘子菌", "Kiss墨瞳", "leonaaaaa", "时妖", "牛奶你个面包", 
  "undfined", "ruuuuuuing", "奔博儿灞", "lambert0415", "锦瑟年华"
];
function randomTime() {
  return 1 + parseInt(Math.random() * 60);
}

function randomCity() {
  var index = parseInt(Math.random() * data.length);
  return data[index];
}


function randomNickName() {
  var index = parseInt(Math.random() * nickNames.length);
  return nickNames[index];
}

function randomCode(len, dict) {
  for (var i = 0, rs = ''; i < len; i++)
    rs += dict.charAt(Math.floor(Math.random() * 100000000) % dict.length);
  return rs;
}
function timeConversion(str) {
  var dateee = new Date(str).toJSON();
  var date = new Date(+new Date(dateee) + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')
  return date
}

function randomPhone() {
  return [1, randomCode(1, '358'), randomCode(1, '0123456789'), '****', randomCode(4, '0123456789')].join('');
}

function base64src(name, base64data, cb) {
  return new Promise((resolve, reject) => {
    const FILE_BASE_NAME = name;
    let filePath = `${wx.env.USER_DATA_PATH}/${FILE_BASE_NAME}`;
    wx.getFileSystemManager().writeFile({
      filePath: filePath,
      data: base64data,
      encoding: 'base64',
      success: () => {
        console.log('写入成功, 路径: ', filePath);
        resolve(filePath);
      },
      fail: err => {
        reject('写入失败：', err);
      },
    });
  })
}
function onDeletePic(path){
  return new Promise((resolve, reject) => {
    var filePath = `${wx.env.USER_DATA_PATH}/${path}`;
    wx.getFileSystemManager().unlink({
      filePath: filePath,
      success: res => {
        console.log('删除成功, 路径: ', filePath);
        resolve(filePath);
      },
      fail: err => {
        reject('删除失败', err);
      }
    })
  })
}
const constants = {
  URL: "https://www.baowujia.com/",
  USERINFO: "userInfo",
  TOKEN: "token",
  CODE: "code",
  APP_SOURCE_DOUYIN: 1,
  APP_SOURCE_TOUTIAO: 0,
  APP_SOURCE_TOOLS: 2
}

module.exports = {
  base64src:base64src,
  onDeletePic:onDeletePic,
  randomTime: randomTime,
  randomCity: randomCity,
  randomNickName: randomNickName,
  randomCode: randomCode,
  fenToYuan: fenToYuan,
  constants: constants,
}