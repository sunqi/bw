# 黑白照片上色 图片模糊处理成清晰
>使用到的技术 微信开发平台的云开发 以及第三方Api [百度Api](https://ai.baidu.com/)
### 配置
1. 云函数的配置
请先了解小程序云开发
2. 百度key的配置
想审核ai平台的权限。需要[黑白图像上色]和【图像清晰度增强】,[图片安全检查]这三个接口权限。 在云函数 getBaiduAccessToken的index.js中 把apiKey 替换为百度ai申请的appkey 把 secretKey替换成为 百度ai审核的secret秘钥然后更新云函数即可。
3. 云数据库的配置
首先要创建token，index，user,tradeOrder四个数据集

### 其他问题
1. 简单的问题自行百度。如果还是不明白请再次百度，最后可以加群讨论谢谢。
2. 联系方式 https://gitee.com/sunqi/personal-contact-information
3. 欢迎在下面留言

### 域名
aip.baidubce.com
### 申明 此项目不得作为商业项目

### 项目展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0925/131458_5edeedec_15968.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0925/131516_cd7e6917_15968.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0925/131530_f5b69b3e_15968.png "3.png")